<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast1')->insert([
            'name' =>$request->input('name'),
            'age' =>$request->input('age'),
            'bio' =>$request->input('bio'),
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast1')->get();

        return view('cast.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $casts = DB::table('cast1')->find($id);

        return view('cast.datail', ['casts' => $casts ]);
    }

    public function edit($id)
    {
        $casts = DB::table('cast1')->find($id);

        return view('cast.edit', ['casts' => $casts ]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast1')
            -where('id', $id)
            -update(
                [
                    'name' => $request->input('name'),
                    'age'  => $request->input('age'),
                    'bio'  => $request->input('bio'),

                ]
            );

        return redirect('/cast');

    }

    public function destroy($id)
    {
        DB::table('cast1')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }

}
