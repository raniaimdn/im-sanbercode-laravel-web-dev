@extends('layouts.dashboard')
@section('title')
Register
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf   
        <label>First Name :</label> <br> <br>
        <input type="text" name="FirstName"> <br> <br>
        <label>Last Name :</label> <br> <br>
        <input type="text" name="LastName"> <br> <br>
        <label>Gender :</label> <br> <br>
        <input type="radio" value="1" name="Gender"> Male <br>
        <input type="radio" value="2" name="Gender"> Female <br>
        <input type="radio" value="3" name="Gender"> Other <br> <br>
        <label>Nationality :</label> <br> <br>
        <select name="Nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option>
        </select> <br> <br>
        <label>Language Spoken :</label> <br> <br>
        <input type="checkbox" value="1" name="LanguageSpoken"> Bahasa Indonesia <br>
        <input type="checkbox" value="2" name="LanguageSpoken"> English <br>
        <input type="checkbox" value="3" name="LanguageSpoken"> Other <br> <br>
        <label>Bio :</label> <br> <br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection