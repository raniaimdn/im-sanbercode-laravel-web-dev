@extends('layouts.dashboard')

@section('title')
Edit Cast
@endsection
@section('content')
<form action="/cast/{{$casts->id}}" method="POST">
    @csrf 
    @method('PUT')
  <div class="form-group">
    <label>Cast Name</label>
    <input type="text" name="name" value="{{$casts->name}}" class="@error('name') is-valid @enderror form-control"> 
  </div>
  @error('name')
     <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Cast Age</label>
    <input type="text" name="age" value="{{$casts->age}}" class="@error('age') is-valid @enderror form-control">
  </div>
  @error('age')
     <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Cast Bio</label>
    <textarea name="bio" class="@error('bio') is-valid @enderror form-control" cols="30" rows="10">{{$casts->bio}}</textarea>
  </div>
  @error('bio')
     <div class="alert alert-danger">{{$message}}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection